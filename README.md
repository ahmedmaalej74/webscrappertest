# Web Scrapper

### Présentation
Cette application présente une démonstration de scrapping d'un [site Web](https://bookboon.com/fr) en Java et Selenium.

### Fonctionnalités (Scénario)
1. Rechercher un livre
2. Selectionner le premier livre de la liste
3. Remplir le formulaire de téléchargement
4. Télécharger le fichier

### Important
Le scrapping est fait en utilisant Selenium et le <strong>driver de Firefox (Linux 32)</strong>.
* Pour garder le driver Firefox mais changer l'OS ou l'architecture 64, il faut remplacer le fichier suivant webdriver/geckodriver.
* Pour changer le type du driver, il faut mettre dans une première étape le driver executable dans le dossier webdriver/geckodriver puis adapter le code.

### Exécution
<code>mvn clean install exec:java</code><br><br>
> L'instruction ci-dessus permet de compiler, packager le projet (jar) et exécuter finalement la classe Main.

