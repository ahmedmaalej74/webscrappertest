package others.test2;

import io.alphonse.scrapper.Utils;

public final class TestCase2 extends Thread {

	@Override
	public void run() {
		// Firefox driver
		System.setProperty(Utils.DRIVER_NAME, Utils.DRIVER_PATH);

		// Chrome driver
		// System.setProperty("webdriver.chrome.driver", "webdriver/chromedriver");

		TwitterScrapper webSrcapper = new TwitterScrapper();
		webSrcapper.openTestSite();
		webSrcapper.login("ahmedmaalej74", "250792");
		webSrcapper.search("Tunisia");
		// webSrcapper.logout();
		webSrcapper.closeBrowser();
	}

	public static void main(String[] args) {
		(new TestCase2()).start();
	}
}
