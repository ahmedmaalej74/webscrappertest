package others.test3;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.alphonse.scrapper.Utils;

public class GoogleScrapper {
	private static final String URL = "https://www.google.com/";
	private static final Integer TIME_OUT = 8;

	private final WebDriver driver;
	private final WebDriverWait wait;

	public GoogleScrapper() {
		driver = new FirefoxDriver(Utils.getFirefoxDriverProfile());
		wait = new WebDriverWait(driver, TIME_OUT);
	}

	/**
	 * Open the test website.
	 */
	public void openTestSite() {
		driver.navigate().to(URL);
	}

	/**
	 * 
	 * @param username
	 * @param Password
	 * 
	 *                 Logins into the website, by entering provided username and
	 *                 password
	 */
	public void search(String word) {

		WebElement searchEditbox = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//form[@role='search']//input[@name='q' and @type='text']")));
		searchEditbox.sendKeys(word);

		WebElement searchButton = driver
				.findElement(By.xpath("//form[@role='search']//input[@type='submit' and @name='btnK']"));
		searchButton.click();
	}

	/**
	 * 
	 * @param username
	 * @param Password
	 * 
	 *                 Logins into the website, by entering provided username and
	 *                 password
	 * @throws IOException
	 */
	public void downloadFirstFile() {

		Utils.waitForLoad(driver);

		List<WebElement> searchResults = driver
				.findElements(By.xpath("//div[@class='g' and //span/text()='[DOC]']//a"));
		URL url;
		for (WebElement document : searchResults) {
			try {
				url = new URL(document.getAttribute("href"));
				FileUtils.copyURLToFile(url, new File(Utils.DOWNLOAD_PATH + FilenameUtils.getName(url.getPath())));
			} catch (IOException io) {
				System.err.println(io);
			}
		}
	}

	public void closeBrowser() {
		driver.close();
	}
}