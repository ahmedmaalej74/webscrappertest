package others.test3;

import io.alphonse.scrapper.Utils;

public final class TestCase3 extends Thread {

	@Override
	public void run() {
		// Setting Firefox driver
		System.setProperty(Utils.DRIVER_NAME, Utils.DRIVER_PATH);

		GoogleScrapper webSrcapper = new GoogleScrapper();
		webSrcapper.openTestSite();
		webSrcapper.search("sample doc");
		webSrcapper.downloadFirstFile();
		// webSrcapper.logout();
		webSrcapper.closeBrowser();
	}

	public static void main(String[] args) {
		(new TestCase3()).start();
	}
}
