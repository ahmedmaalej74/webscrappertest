package io.alphonse.scrapper;

import java.io.IOException;
import java.util.Properties;

public final class SelectorsLoader {
	private static final String PROPERTIES_FILE = "/selectors.properties";
	private static Properties selectors = new Properties();
	public static final String SELECTOR_SEARCH_INPUT = "book.search.input";
	public static final String SELECTOR_SEARCH_ACTION = "book.search.action";
	public static final String SELECTOR_SEARCH_RESULTS = "book.search.results";

	public static final String SELECTOR_FORM_QUESTION1 = "book.download.form.question1";
	public static final String SELECTOR_FORM_QUESTION2 = "book.download.form.question2";
	public static final String SELECTOR_FORM_QUESTION3 = "book.download.form.question3";
	public static final String SELECTOR_FORM_QUESTION4 = "book.download.form.question4";
	public static final String SELECTOR_FORM_DOWNLOAD = "book.download.form.download";

	static {
		try {
			selectors.load(SelectorsLoader.class.getResourceAsStream(PROPERTIES_FILE));
		} catch (IOException e) {
			System.err
					.println("Une erreur est survenue lors du chargement du fichier de configuration des selecteurs..");
		}
	}

	public static String getProperty(String key) {
		return selectors.getProperty(key);
	}
}
