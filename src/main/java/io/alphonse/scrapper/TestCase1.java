package io.alphonse.scrapper;

public final class TestCase1 extends Thread {

	@Override
	public void run() {
		// Firefox driver
		System.setProperty(Utils.DRIVER_NAME, Utils.DRIVER_PATH);

		// Chrome driver
		// System.setProperty("webdriver.chrome.driver", "webdriver/chromedriver");

		Scrapper webSrcapper = new Scrapper();
		webSrcapper.openTestSite();
		webSrcapper.searchBook("Développez votre potentiel");
		webSrcapper.selectFirstBook();
		webSrcapper.completeFormAndDownloadBook("Employé(e)", "Conseil", "Autres");
		webSrcapper.closeBrowser();
	}

	public static void main(String[] args) {
		(new TestCase1()).start();
	}
}
