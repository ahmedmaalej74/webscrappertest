package io.alphonse.scrapper;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public final class Utils {

	// Firefox driver
	public static final String DRIVER_NAME = "webdriver.gecko.driver";
	public static final String DRIVER_PATH = "webdriver/geckodriver";

	// Chrome driver
	// public static final String DRIVER_NAME = "webdriver.chrome.driver";
	// public static final String DRIVER_PATH = "webdriver/chromedriver";

	public static final String DOWNLOAD_PATH = "output/";

	public static void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

	public static FirefoxOptions getFirefoxDriverProfile() {
		FirefoxOptions options = new FirefoxOptions().addPreference("browser.download.folderList", 2)
				.addPreference("browser.download.manager.showWhenStarting", false)
				.addPreference("browser.download.dir", DOWNLOAD_PATH)
				.addPreference("browser.helperApps.neverAsk.openFile",
						"text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml")
				.addPreference("browser.helperApps.neverAsk.saveToDisk",
						"text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml")
				.addPreference("browser.helperApps.alwaysAsk.force", false)
				.addPreference("browser.download.manager.alertOnEXEOpen", false)
				.addPreference("browser.download.manager.focusWhenStarting", false)
				.addPreference("browser.download.manager.useWindow", false)
				.addPreference("browser.download.manager.showAlertOnComplete", false)
				.addPreference("browser.download.manager.closeWhenDone", false).addPreference("pdfjs.disabled", true)
				.addPreference("plugin.disable_full_page_plugin_for_types",
						"application/pdf,application/vnd.adobe.xfdf,application/vnd.fdf,application/vnd.adobe.xdp+xml");
		return options;
	}
}
