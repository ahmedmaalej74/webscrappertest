package io.alphonse.scrapper;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Scrapper {
	private static final String URL = "https://bookboon.com/fr";
	private static final Integer TIME_OUT = 8;

	private final WebDriver driver;
	private final WebDriverWait wait;

	public Scrapper() {
		driver = new FirefoxDriver(Utils.getFirefoxDriverProfile());
		wait = new WebDriverWait(driver, TIME_OUT);
	}

	/**
	 * Open the test website.
	 */
	public void openTestSite() {
		driver.navigate().to(URL);
	}

	/**
	 * Search for a specifc book by title and submits form
	 * 
	 * @param bookTitle
	 */
	public void searchBook(String bookTitle) {

		WebElement searchInput = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_SEARCH_INPUT))));
		searchInput.sendKeys(bookTitle);

		WebElement searchAction = driver
				.findElement(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_SEARCH_ACTION)));

		searchAction.click();
	}

	/**
	 * Select the first book from search result
	 * 
	 */
	public void selectFirstBook() {

		WebElement firstBookLink = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_SEARCH_RESULTS))));
		firstBookLink.click();
	}

	/**
	 * Select the first book from search result
	 * 
	 */
	public void completeFormAndDownloadBook(String category, String profession, String domain) {

		WebElement categoryQuestion = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_FORM_QUESTION1))));
		categoryQuestion.sendKeys(category);
		categoryQuestion.sendKeys(Keys.ENTER);

		WebElement professionQuestion = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_FORM_QUESTION2))));
		professionQuestion.sendKeys(profession);
		professionQuestion.sendKeys(Keys.ENTER);

		WebElement domainQuestion = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_FORM_QUESTION3))));
		domainQuestion.sendKeys(domain);
		domainQuestion.sendKeys(Keys.ENTER);

		// Select No for Newsletter question
		WebElement emailQuestion = driver
				.findElement(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_FORM_QUESTION4)));

		Actions builder = new Actions(driver);
		builder.moveToElement(emailQuestion).perform();
		emailQuestion.click();

		WebElement downloadButton = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(SelectorsLoader.getProperty(SelectorsLoader.SELECTOR_FORM_DOWNLOAD))));
		downloadButton.click();
	}

	public void closeBrowser() {
		driver.close();
	}

}